//
//  Fuel_LogUITests.swift
//  Fuel LogUITests
//
//  Created by The App Experts on 05/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import XCTest

class Fuel_LogUITests: XCTestCase {
    
    let app = XCUIApplication()


    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFirstViewControllerIs_FuelLog(){
        let fuelLogLabel = app.navigationBars["Fuel Log"]
        XCTAssertTrue(fuelLogLabel.exists)
    }
    
    func testAddTapGoesToAddEntryViewController(){
        app.navigationBars["Fuel Log"].buttons["Add"].tap()
        let newEntry = app.navigationBars["New Entry"]
        
        XCTAssertTrue(newEntry.exists)
    }
    
    func testNewEntryViewControllersSaveFunctionality() {
        //let app = XCUIApplication()
        app.navigationBars["Fuel Log"].buttons["Add"].tap()
        app.textFields["odometer"].tap()

        let pricePLTextField = app.textFields["Price P/L"]
        pricePLTextField.tap()
        pricePLTextField.tap()

        let litresTextField = app.textFields["litres"]
        litresTextField.tap()
        litresTextField.tap()
        app.textFields["Total Cost"].tap()
        pricePLTextField.tap()
        pricePLTextField.tap()
        app.navigationBars["New Entry"].buttons["Save"].tap()

    }
    
 

}


