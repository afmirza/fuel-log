//
//  ReminderDetailsViewController.swift
//  Fuel Log
//
//  Created by The App Experts on 13/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit
import EventKit

class ReminderDetailsViewController: UIViewController {
    
    var datePicker: UIDatePicker!
    var reminder: EKReminder!
    var eventStore: EKEventStore!
    var dateFormatter: DateFormatter!
    
    //MARK:- Outlets
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var reminderTextView: UITextField!
    
    //MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reminderTextView.text = reminder.title
        
        dateFormatter = DateFormatter()
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        dateFormatter.dateFormat = "dd-MM-yyyy-hh:mm a"
        
        datePicker = UIDatePicker()
        datePicker.datePickerMode = .dateAndTime
        datePicker.locale = .current
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(gesture:)))
        view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
        datePicker.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
        dateTextField.inputView = datePicker
        reminderTextView.becomeFirstResponder()

    }
//MARK:- Actions
    @IBAction func saveReminder(_ sender: Any) {
        reminder.title = reminderTextView.text
        guard let date = dateTextField.text, !date.isEmpty, (dateFormatter.date(from: date) != nil) else {
            errorLabel.text = "Enter a valid date format"
            return}
        let alarm = EKAlarm(absoluteDate: dateFormatter.date(from: date)!)
        reminder.addAlarm(alarm)
        let datecom = NSCalendar.current.dateComponents([.day, .month, .year], from: dateFormatter.date(from: date)!)
        
        reminder.calendar = self.eventStore.defaultCalendarForNewReminders()
        reminder.dueDateComponents = datecom
        do {
            try eventStore.save(reminder, commit: true)
            
            self.navigationController?.popViewController(animated: true)
        }catch{
            print("Error creating and saving new reminder : \(error)")
        }
        
    }
    
    
    @objc func dateChanged(){
        
        dateTextField.text = dateFormatter.string(from: datePicker.date)
//        view.endEditing(true)
    }
    
    @objc func handleTapGesture(gesture: UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    
}
