//
//  RemindersViewController.swift
//  Fuel Log
//
//  Created by The App Experts on 12/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit
import EventKit

class RemindersViewController: UIViewController {
    var eventStore: EKEventStore!
    var reminders: [EKReminder]!
    var selectedReminder: EKReminder!
    @IBOutlet weak var tableView: UITableView!
    
//MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let observer = NotificationCenter.default
        observer.addObserver(self, selector: #selector(appBecomeActiveBackground), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        tableView.reloadData()
        tableView.dataSource = self
        tableView.delegate = self

    }
    


    override func viewWillAppear(_ animated: Bool) {
        
// Asking user persmission and getting reminders from the eventkit database
        eventStore = EKEventStore()
        reminders = [EKReminder]()
        eventStore.requestAccess(to: EKEntityType.reminder){
            (granted: Bool, error: Error?) -> Void in
            if granted{
                let predicate = self.eventStore.predicateForReminders(in: nil)
                self.eventStore.fetchReminders(matching: predicate, completion: {
                    (reminders: [EKReminder]?) -> Void in
                    self.reminders = reminders
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                })
            }else {
                let alertView = UIAlertController(title: "Unable to Get Reminders Data",
                                                  message: "The app is not permitted to acces reminders, make sure to grant persmission in the settings and try again",
                                                  preferredStyle:. alert)
                let okAction = UIAlertAction(title: "Try Again", style: .default)
                alertView.addAction(okAction)
                self.present(alertView, animated: true)

            }
        }
    }

//MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showReminderDetails"{
            let reminderDetailsVC = segue.destination as! ReminderDetailsViewController
            reminderDetailsVC.reminder = selectedReminder
            reminderDetailsVC.eventStore = eventStore
        }else{
            let newReminderVC = segue.destination as! NewReminderViewController
            newReminderVC.eventStore = eventStore
        }
    }
    
//MARK:- App State
    @objc func appBecomeActiveBackground() {

        tableView.reloadData()
    }

//MARK:- Actions
    @IBAction func addAction(_ sender: Any) {
        performSegue(withIdentifier: "addReminder", sender: self)
    }
    
    
}

//MARK:- Table view delegates

extension RemindersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reminders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reminderCell", for: indexPath)
        let reminder = reminders[indexPath.row]
        cell.textLabel?.text = reminder.title
        let formatter:DateFormatter = DateFormatter()
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        formatter.dateFormat = "dd-MM-yyyy-hh:mm a"
        if let dueDate =  reminder.alarms?.last?.absoluteDate{
            cell.detailTextLabel?.text = formatter.string(from: dueDate)
        }else{
            cell.detailTextLabel?.text = "N/A"
        }
        cell.layer.borderWidth = 2.0
        cell.layer.cornerRadius = 10
        return cell
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        selectedReminder = reminders[indexPath.row]
        performSegue(withIdentifier: "showReminderDetails", sender: self)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        let reminder: EKReminder = reminders[indexPath.row]
        do{
            try eventStore.remove(reminder, commit: true)
            reminders.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
        }catch{
            print("An error occurred while removing the reminder from the Calendar database: \(error)")
        }
    }
    
    
}
