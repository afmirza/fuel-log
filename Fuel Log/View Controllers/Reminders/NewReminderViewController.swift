//
//  NewReminderViewController.swift
//  Fuel Log
//
//  Created by The App Experts on 12/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit
import EventKit

class NewReminderViewController: UIViewController {
    
    var eventStore: EKEventStore!
    var dateFormatter: DateFormatter!
    var datePicker: UIDatePicker!
    @IBOutlet weak var reminderTitleTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter = DateFormatter()
        
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        dateFormatter.dateFormat = "dd-MM-yyyy-hh:mm a"
        datePicker = UIDatePicker()
        datePicker.datePickerMode = .dateAndTime
        datePicker.locale = .current
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(gesture:)))
        view.addGestureRecognizer(tapGesture)

        datePicker.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
        dateTextField.inputView = datePicker
        reminderTitleTextField.becomeFirstResponder()
        dateTextField.text = dateFormatter.string(from: Date())
    }
    
    //MARK:- Actions
    
    @objc func handleTapGesture(gesture: UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    
    @IBAction func addReminderAction(_ sender: UIBarButtonItem) {
        
        let reminder = EKReminder(eventStore: self.eventStore)
        reminder.title = reminderTitleTextField.text
        guard let date = dateTextField.text, !date.isEmpty, (dateFormatter.date(from: date) != nil) else {
            errorLabel.text = "Enter a valid date format"
            return}
        
        let datecom = NSCalendar.current.dateComponents([.day, .month, .year], from: dateFormatter.date(from: date)!)
        let alarm = EKAlarm(absoluteDate: dateFormatter.date(from: date)!)
        reminder.addAlarm(alarm)

        reminder.calendar = self.eventStore.defaultCalendarForNewReminders()
        reminder.dueDateComponents = datecom
        do {
            try eventStore.save(reminder, commit: true)

             self.navigationController?.popViewController(animated: true)
        }catch{

            showFailedAlert(error: error)
        }
    }
    
    
    @objc func dateChanged(){
        
        dateTextField.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    @IBAction func cancelAction(_ sender: UIBarButtonItem) {

         self.navigationController?.popViewController(animated: true)
    }

    //MARK:- Alert
    private func showFailedAlert(error: Error) {
         let alertView = UIAlertController(title: "Error creating and saving new reminder",
                                           message: "Make sure you give app premission via setting \(error.localizedDescription)",
                                           preferredStyle:. alert)
         let okAction = UIAlertAction(title: "Foiled Again!", style: .default)
         alertView.addAction(okAction)
         present(alertView, animated: true)
         
     }
    
}
