//
//  ViewController.swift
//  Fuel Log
//
//  Created by The App Experts on 04/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData


class ViewController: UIViewController {
    
    var isAuthenticated = false
    var dateFormatter: DateFormatter!
    // Create a reference to the CoreData Stack
    //    private let cdStack = CoreDataController.shared
    let model = FuelModel(CoreDataController.shared)
    
    // The all mighty FetchedResultsController
    fileprivate lazy var fetchedResultsController: NSFetchedResultsController <Fuel> = {
        
        let fetchRequest:NSFetchRequest =  Fuel.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        
        //managedObjectContext is your instance of NSManagedObjectContext
        let fetchedResultsContoller = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                 managedObjectContext: CoreDataController.shared.mainContext,
                                                                 sectionNameKeyPath: nil,
                                                                 cacheName: nil)
        fetchedResultsContoller.delegate = self
        return fetchedResultsContoller
    }()
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            try fetchedResultsController.performFetch()
        } catch let fetchError {
            print("Fetch Error: \(fetchError.localizedDescription)")
        }
        
        tableView.dataSource = self
        tableView.delegate = self
//        tableView.rowHeight = 100
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        dateFormatter = DateFormatter()
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        dateFormatter.dateFormat = "dd-MM-yyyy:hh:mm a"
        locationManager.requestAlwaysAuthorization()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // request to acces location services
        locationManager.requestAlwaysAuthorization()
        showLoginView()
    }
    
    //MARK:- Actions
    
    @IBAction func logoutAction(_ sender: Any) {
        isAuthenticated = false
        performSegue(withIdentifier: "loginView", sender: self)
    }
    
    
    @IBAction func unwindSegue(_ segue: UIStoryboardSegue) {
        
        isAuthenticated = true
        view.alpha = 1.0
    }
    
    func showLoginView() {
        if !isAuthenticated {
            performSegue(withIdentifier: "loginView", sender: self)
        }
    }

    
    @IBAction func plusAction(_ sender: Any) {
        performSegue(withIdentifier: "newEntry", sender: nil)
    }
    
    
    
}



// MARK: -   TableView Delegate methods

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deletAction = UITableViewRowAction(style: .destructive, title: "Remove"){
            (action, indexPath) in
            let objToDelete = self.fetchedResultsController.object(at: indexPath)
            self.fetchedResultsController.managedObjectContext.delete(objToDelete)
            self.model.coreDataController.saveContext()
            
        }
        
        return[deletAction]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        guard let sections = fetchedResultsController.sections else { fatalError("FRC not configuired correctly") }
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let sections = fetchedResultsController.sections else { fatalError("FRC not configuired correctly") }
        
        let sectionInformation = sections[section]
        return sectionInformation.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? FuelTableViewCell else {return UITableViewCell()}
    
        let fuelLog = fetchedResultsController.object(at: indexPath)
        let dateString = dateFormatter.string(from: fuelLog.date!)
        
        cell.odometerLabel.text = String(fuelLog.odometer)
        cell.totalLabel.text = String(fuelLog.cost)
        cell.dateLabel.text = dateString
        cell.locationLabel.text = fuelLog.locationName
        cell.mileageLabel.text = String(format: "%.4f", fuelLog.mileage)
        cell.layer.borderWidth = 2.0
        cell.layer.cornerRadius = 10
        
        return cell
    }
    
}


// MARK: - Fetch Results Controller Stuff

extension ViewController : NSFetchedResultsControllerDelegate {
    
//    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, sectionIndexTitleForSectionName sectionName: String) -> String? {
//        return String(sectionName.hashValue)
//        //        return String("Refueling")
//    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        @unknown default:
            fatalError()
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
        case .move:
            if let indexPath = indexPath, let newIndexPath = newIndexPath {
                tableView.moveRow(at: indexPath, to: newIndexPath)
            }
        @unknown default:
            fatalError("@unknown default")
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}

