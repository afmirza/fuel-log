//
//  NewEntryViewController.swift
//  Fuel Log
//
//  Created by The App Experts on 05/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

class NewEntryViewController: UIViewController {
    
    let model = FuelModel(CoreDataController.shared)
    let locationManager = CLLocationManager()
    
    var locationCordinate = LocationComponents(lat: 0, lng: 0)
    var dateFormatter: DateFormatter!
    var previousOdometerReading: Double!
    
    var datePicker: UIDatePicker!
    
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var lastOdometerReadingLabel: UILabel!
    @IBOutlet weak var odometerTextField: UITextField!
    @IBOutlet weak var pricePerLitreTextField: UITextField!
    @IBOutlet weak var totalCostTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var litresTextField: UITextField!
    
    //MARK:- View life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter = DateFormatter()
        
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        dateFormatter.dateFormat = "dd-MM-yyyy-hh:mm a"
        
        // Date Picker As Input for Date Text Field
        datePicker = UIDatePicker()
        datePicker.datePickerMode = .dateAndTime
        datePicker.locale = .current
        dateTextField.inputView = datePicker
        datePicker.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
        // setup input keyboards for each textfield
        setupKeyBoardInputMethods()
        
        view.backgroundColor = .lightGray
        
        //setting delegates for relevant textfields
        pricePerLitreTextField.delegate = self
        litresTextField.delegate = self
        odometerTextField.delegate = self
        
        searchBar.delegate = self
        searchBar.placeholder = "Search via Post Code"
        
        //tap gesture to dismiss datepicker by taping anywhere on screen
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(gesture:)))
        view.addGestureRecognizer(tapGesture)
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressGesture(gesture:)))
        longPressGesture.minimumPressDuration = 3
        longPressGesture.numberOfTouchesRequired = 1
        longPressGesture.numberOfTapsRequired = 1
        view.addGestureRecognizer(longPressGesture)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // function finds device current location when view appears
        reverseGeoCoding()
        previousOdometerReading =  fetchLastOdometerReading()
        dateTextField.text = dateFormatter.string(from: Date())
    }
    
    @objc func handleTapGesture(gesture: UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    @objc func handleLongPressGesture(gesture: UILongPressGestureRecognizer){
        if gesture.state == .began {
          
            model.network.getWeather(currentLocation: locationCordinate) { (weather, error) in
                guard let weatherInfoMain = weather?.weather.first?.main, let weatherInfoDescription = weather?.weather.first?.description else {return}
                DispatchQueue.main.async {
                     let alertView = UIAlertController(title: "Today's Weather Info for your location",
                                                                     message: "\(weatherInfoMain), \(weatherInfoDescription)",
                                                                     preferredStyle:. alert)
                                   let okAction = UIAlertAction(title: "Be Safe", style: .default)
                                   alertView.addAction(okAction)
                                   self.present(alertView, animated: true)
                }
               
            }
          
        }
    }
    
    @objc func dateChanged(){
        dateTextField.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text, text.count > 0 else {return}
        // check for valid post code for a fuel station
        if validatePostCode(textForValidation: text){
            self.findLocation(searchAddress: text)
            resignFirstResponder()
        }else {
            errorLabel.text = "Please Enter a valid Post Code"
        }
    }
// function to check if entered date is in valid format
    func validatePostCode(textForValidation text: String) -> Bool {
        
        let postCodeRegEx = "^[a-zA-Z]{1,2}([0-9]{1,2}|[0-9][a-zA-Z])\\s*[0-9][a-zA-Z]{2}$"
        
        let postCodePred = NSPredicate(format:"SELF MATCHES %@",  postCodeRegEx)
        return postCodePred.evaluate(with: text)
        
    }
    
    func setupKeyBoardInputMethods(){
        
        pricePerLitreTextField.keyboardType = .decimalPad
        litresTextField.keyboardType = .decimalPad
        odometerTextField.keyboardType = .decimalPad
    }
    
    
    
    // function to get odometer reading of the most recent Fuel entry to CoreData
    func fetchLastOdometerReading() -> Double?{
        
        let result = model.fetchAllRecords()
        guard let lastValue = result.first as? Fuel else {
            lastOdometerReadingLabel.text = nil
            return nil
        }
        lastOdometerReadingLabel.text = String(format: "%.4f",lastValue.odometer)
        return lastValue.odometer
    }
    
    func fetchLastDate() -> Date? {
        let result = model.fetchAllRecords()
        guard let lastDateEntry = result.first as? Fuel else {
            return nil
        }
        
        return lastDateEntry.date
    }
    
    //MARK:- Buton Actions
    @IBAction func saveAction(_ sender: Any) {
        // validating inputs
        guard let date1 = dateTextField.text, !date1.isEmpty, (dateFormatter.date(from: date1) != nil) else {
            errorLabel.text  =  "Enter a valid date"
            return
        }
        
        guard let pricePerLitreString = pricePerLitreTextField.text, !pricePerLitreString.isEmpty else {
            errorLabel.text = "Eter value for price per litre"
            return
        }
        
        guard let odometerString = odometerTextField.text, !odometerString.isEmpty else {
            errorLabel.text = "Enter a valid value for odometer reading"
            return
        }
        
        guard let litersString = litresTextField.text, !litersString.isEmpty else {
            errorLabel.text = "Enter value for number of liters"
            return
        }
        
        guard let totalCostString = totalCostTextField.text, !totalCostString.isEmpty else {
            errorLabel.text = "Total cost is empty"
            return
        }
        
        let pricePerLitre = (pricePerLitreString as NSString).doubleValue
        let litre = (litersString as NSString ).doubleValue
        let currentOdometerValue = (odometerString as NSString).doubleValue
        let totalCost = (totalCostString as NSString).doubleValue
        guard let date = dateFormatter.date(from: date1) else {return}
        
        let dateForPreviousLog = fetchLastDate()
        let previousDate = dateForPreviousLog ?? dateFormatter.date(from: "01-01-1900-01:01 a")
        
        // will restrict new entries based on most recent entry's date and odometer reading
        if previousOdometerReading == nil{
            
            let fuelLog = FuelLog(pricePerLitre: pricePerLitre, litres: litre, cost: totalCost, odometer: currentOdometerValue, previousOdometerValue: nil, date: date, locationName: locationTextField.text!, locationCordinates: locationCordinate)
            model.logFuel(item: fuelLog)
            dismissView()
            return
        }else if currentOdometerValue > previousOdometerReading && date > previousDate! {
            let fuelLog = FuelLog(pricePerLitre: pricePerLitre, litres: litre, cost: totalCost, odometer: currentOdometerValue, previousOdometerValue: previousOdometerReading, date: date, locationName: locationTextField.text!, locationCordinates:  locationCordinate)
            model.logFuel(item: fuelLog)
            dismissView()
        }else{
            errorLabel.text = "Please check odometer and date values"
        }
        
    }
    
    
    func dismissView(){
        self.navigationController?.popViewController(animated: true)
    }
    
    // function sets delegate and requests device location to populate location textfield
    func reverseGeoCoding(){
        locationManager.delegate = self
        locationManager.requestLocation()
        
    }
    
    //function to find fuel station Name with post code given in search Bar also talks to google maps api
    func findLocation(searchAddress: String){
        
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(searchAddress){(placemarks, error) in
            guard let placemark = placemarks?.first,
                let coordinate = placemark.location?.coordinate else {
                    print("\(error?.localizedDescription)")
                    return }
            
            self.locationCordinate.lat = coordinate.latitude
            self.locationCordinate.lng = coordinate.longitude
            self.model.network.search(self.locationCordinate){locationName, error in
                DispatchQueue.main.async {
                    if let fuelSation = locationName?.name {
                        self.locationTextField.text = fuelSation
                        
                    }else {
                        
                        let alertView = UIAlertController(title: "No Fuel Station Found",
                                                          message: "Please check Post Code",
                                                          preferredStyle:. alert)
                        let okAction = UIAlertAction(title: "Try Again", style: .default)
                        alertView.addAction(okAction)
                        self.present(alertView, animated: true)
                    }
                    
                }
            }
        }
    }
    

    
    func showFailedAlert() {
        let alertView = UIAlertController(title: "Unable to Get Location",
                                          message: "Please allow app to get location via settings",
                                          preferredStyle:. alert)
        let okAction = UIAlertAction(title: "Try Again", style: .default)
        alertView.addAction(okAction)
        present(alertView, animated: true)
        
    }
    
}


//MARK:- Text Field Delegates
extension NewEntryViewController: UITextFieldDelegate, UISearchBarDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        // Update Total cost text field fro pricePerLItreTextField
        if textField == pricePerLitreTextField {
            guard let pricperlitre = pricePerLitreTextField.text, !pricperlitre.isEmpty else {
                return
            }
            
            guard let liters = litresTextField.text, !liters.isEmpty  else{
                
                return
            }
            let resultTotalCost = (pricperlitre as NSString).doubleValue * (liters as NSString).doubleValue
            
            totalCostTextField.text = String (resultTotalCost)
        }
        
        if textField == litresTextField {
            guard let pricperlitre = pricePerLitreTextField.text, !pricperlitre.isEmpty else {
                return
            }
            
            guard let liters = litresTextField.text, !liters.isEmpty else {
                
                return
            }
            let resultTotalCost = (pricperlitre as NSString).doubleValue * (liters as NSString).doubleValue
            
            totalCostTextField.text = String (resultTotalCost)
        }
        
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // User pressed the delete-key to remove a character, this is always valid, return true to allow change
        if string.isEmpty { return true }
        
        let currentText = textField.text ?? ""
        let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        return replacementText.isValidDouble(maxDecimalPlaces: 4)
        
    }
}


//MARK:- Location Manager Delegates
extension NewEntryViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard  let location = locations.last else {return}
        locationCordinate.lat = location.coordinate.latitude
        locationCordinate.lng =  location.coordinate.longitude
        // passes device location to search function that talks to google map api to get fuel station name within 100 meters of device
        model.network.search(locationCordinate){locationName, error in
            DispatchQueue.main.async {
                self.locationTextField.text = locationName?.name
            }
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager Error: \(error)")
        showFailedAlert()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
    }
}




