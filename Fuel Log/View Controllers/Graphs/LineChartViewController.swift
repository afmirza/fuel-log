//
//  LineChartViewController.swift
//  Fuel Log
//
//  Created by The App Experts on 14/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit
import Charts
import CoreData

class LineChartViewController: UIViewController {
    
    var fuelLogEntries = [Fuel]()

    @IBOutlet weak var chartView: LineChartView!
    
    
    let model = FuelModel(CoreDataController.shared)
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        chartView.noDataText = "No Data Available"
        chartView.drawBordersEnabled = true
        getData()
        updateGraph()
        
    }
    
    // function to get data from the core data
    func getData(){
        guard let result = model.fetchAllRecords() as? [Fuel] else {
                chartView.noDataText = "No data"
            return }
        fuelLogEntries = result.reversed()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        getData()
        updateGraph()
        
    }
    
    func updateGraph(){
        
        var referenceTimeInterval: TimeInterval = 0
        if let minTimeInterval = (fuelLogEntries.map { $0.date!.timeIntervalSince1970 }).min() {
            referenceTimeInterval = minTimeInterval
        }
        
        // Define chart xValues formatter
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.locale = Locale.current
        
        let xValuesNumberFormatter = ChartXAxisFormatter(referenceTimeInterval: referenceTimeInterval, dateFormatter: formatter)
        
        let xaxis = chartView.xAxis
        xaxis.labelCount = fuelLogEntries.count
        xaxis.drawLabelsEnabled = true
        xaxis.drawLimitLinesBehindDataEnabled = true
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.labelTextColor = .red
        chartView.leftAxis.labelTextColor = .green
        chartView.rightAxis.enabled = false
        
        xaxis.valueFormatter = xValuesNumberFormatter
        
        var lineChartEntry = [ChartDataEntry]()
        
        for item in fuelLogEntries{
            if  let timeInterval = item.date?.timeIntervalSince1970 {
                let xValue = (timeInterval - referenceTimeInterval) / (3600 * 24)
                
                let yValue = item.odometer
                let entry = ChartDataEntry(x: xValue, y: yValue)
                lineChartEntry.append(entry)
            }
        }
        
        let line1 = LineChartDataSet(entries: lineChartEntry, label: "Miles Per Refueling")
        line1.colors = [NSUIColor.blue]
        
        let data = LineChartData()
        data.addDataSet(line1)
        chartView.data = data
        chartView.chartDescription?.text = "Line Chart"
        
        
    }

    
}
