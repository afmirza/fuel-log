//
//  BarChartViewController.swift
//  Fuel Log
//
//  Created by The App Experts on 14/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit
import Charts

class BarChartViewController: UIViewController {
    
    var fuelLogEntries = [Fuel]()
    let model = FuelModel(CoreDataController.shared)
    
    @IBOutlet weak var chartView: BarChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        chartView.noDataText = "No Data Available"
        chartView.animate(yAxisDuration: 2.0)
        chartView.pinchZoomEnabled = true
        chartView.drawBarShadowEnabled = false
        chartView.drawBordersEnabled = true
        chartView.doubleTapToZoomEnabled = true
        
        getData()
        updateGraph()
    }

    
    override func viewDidAppear(_ animated: Bool) {
        getData()
        updateGraph()
    }
    
    
    func getData(){
        guard let result = model.fetchAllRecords() as? [Fuel] else {
                chartView.noDataText = "No data"
            return }
        fuelLogEntries = result.reversed()
    }
    
    func updateGraph(){
          
          var referenceTimeInterval: TimeInterval = 0
          if let minTimeInterval = (fuelLogEntries.map { $0.date!.timeIntervalSince1970 }).min() {
              referenceTimeInterval = minTimeInterval
          }
          
          // Define chart xValues formatter
          let formatter = DateFormatter()
          formatter.dateStyle = .short
          formatter.timeStyle = .none
          formatter.dateFormat = "dd/MM/yyyy"
          formatter.locale = Locale.current
          
          let xValuesNumberFormatter = ChartXAxisFormatter(referenceTimeInterval: referenceTimeInterval, dateFormatter: formatter)
          
          let xaxis = chartView.xAxis
          xaxis.labelCount = fuelLogEntries.count
          xaxis.drawLabelsEnabled = true
          xaxis.drawLimitLinesBehindDataEnabled = true
          chartView.xAxis.labelPosition = .bottom
          chartView.xAxis.labelTextColor = .red
          chartView.leftAxis.labelTextColor = .red
          chartView.rightAxis.enabled = false
          
          xaxis.valueFormatter = xValuesNumberFormatter
          
          var barChartEntry = [BarChartDataEntry]()
          
          for item in fuelLogEntries{
              if  let timeInterval = item.date?.timeIntervalSince1970 {
                  let xValue = (timeInterval - referenceTimeInterval) / (3600 * 24 )
                  
                  let yValue = item.mileage
                  let entry = BarChartDataEntry(x: xValue, y: yValue)
                  barChartEntry.append(entry)
              }
          }
          
        let chartDataSet = BarChartDataSet(entries: barChartEntry, label: "Mileage Per Refueling")
        let chartData = BarChartData(dataSet: chartDataSet)
        chartView.data = chartData
        chartView.chartDescription?.text = "Bar Chart"
          
          
      }
    
}
