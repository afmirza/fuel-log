/*!
 This file contains code for ChartViewController, that acts as a container view having LinChartViewController & BarChartViewController as its children
 */

//
//  ChartsViewController.swift
//  Fuel Log
//
//  Created by The App Experts on 08/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit
import Charts
import CoreData

class ChartsViewController: UIViewController {
    

    @IBOutlet weak var segmentedControl: UISegmentedControl!

    
    private lazy var lineChartViewController: LineChartViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "LineChartViewController") as! LineChartViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()

    private lazy var barChartViewController: BarChartViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "BarChartViewController") as! BarChartViewController

        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        updateView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
// MARK:- View Methods
    private func setupView() {
        setupSegmentedControl()
        updateView()
    }
    
    private func updateView() {
        if segmentedControl.selectedSegmentIndex == 0 {
            remove(asChildViewController: barChartViewController)
            add(asChildViewController: lineChartViewController)
        } else {
            remove(asChildViewController: lineChartViewController)
            add(asChildViewController: barChartViewController)
        }
    }
    
    
    private func setupSegmentedControl() {
        // Configure Segmented Control
        segmentedControl.removeAllSegments()
        segmentedControl.insertSegment(withTitle: "Line Chart", at: 0, animated: false)
        segmentedControl.insertSegment(withTitle: "Bar Chart", at: 1, animated: false)
        segmentedControl.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
        
        // Select First Segment
        segmentedControl.selectedSegmentIndex = 0
    }

//MARK:- Actions
    @objc func selectionDidChange(_ sender: UISegmentedControl) {
        updateView()
    }
    
    @IBAction func shareAction(_ sender: Any) {
        screenShotShare()
    }
    
    
// MARK:- Helper Methods
    // Social media sharing
    
    func screenShotShare(){
        let bounds = UIScreen.main.bounds
        UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0.0)
        view.drawHierarchy(in: bounds, afterScreenUpdates: false)
        guard let img = UIGraphicsGetImageFromCurrentImageContext() else {return}
        UIGraphicsEndImageContext()
        let activityViewController = UIActivityViewController(activityItems: [img], applicationActivities: nil)
        activityViewController.excludedActivityTypes = [.saveToCameraRoll, .print, .airDrop]
        self.present(activityViewController, animated: true, completion: nil)
    }

    // MARK: - Helper Methods

    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)

        // Add Child View as Subview
        view.addSubview(viewController.view)

        // Configure Child View
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)

        // Remove Child View From Superview
        viewController.view.removeFromSuperview()

        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
}


/*!
 @brief Custom class to that takes optional DateFormatter and TimeInterval in the constructor

 */
class ChartXAxisFormatter: NSObject {
    fileprivate var dateFormatter: DateFormatter?
    fileprivate var referenceTimeInterval: TimeInterval?
    
    convenience init(referenceTimeInterval: TimeInterval, dateFormatter: DateFormatter) {
        self.init()
        self.referenceTimeInterval = referenceTimeInterval
        self.dateFormatter = dateFormatter
    }
}


extension ChartXAxisFormatter: IAxisValueFormatter {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        guard let dateFormatter = dateFormatter,
            let referenceTimeInterval = referenceTimeInterval
            else {
                return ""
        }
        
        let date = Date(timeIntervalSince1970: value * 3600 * 24 + referenceTimeInterval)
        return dateFormatter.string(from: date)
    }
    
}
