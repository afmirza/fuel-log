    //
    //  LoginViewController.swift
    //  Fuel Log
    //
    //  Created by The App Experts on 09/04/2020.
    //  Copyright © 2020 The App Experts. All rights reserved.
    //
    
    import UIKit
    import Foundation
    import AuthenticationServices
    
    // Keychain Configuration
    struct KeychainConfiguration {
        static let serviceName = "FuelLog"
        static let accessGroup: String? = nil
    }
    
    @available(iOS 13.0, *)
    class LoginViewController: UIViewController {
        
        let createLoginButtonTag = 0
        let loginButtonTag = 1
        var login: Login!
        
        //MARK:- Outlets
        @IBOutlet weak var usernameTextField: UITextField!
        @IBOutlet weak var passwordTextField: UITextField!
        @IBOutlet weak var loginButton: UIButton!
        @IBOutlet weak var stackView: UIStackView!
        var appleButton : ASAuthorizationAppleIDButton!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            login = Login()
            // flag to check if an account exists
            let hasLogin = UserDefaults.standard.bool(forKey: "hasLoginKey")
            let usedAppleID = UserDefaults.standard.bool(forKey: "usedAppleID")
            if hasLogin {
                
                if usedAppleID {
                    usernameTextField.isHidden = true
                    passwordTextField.isHidden = true
                    setupAppleLogin()
                    loginButton.isHidden = true
                } else {
                    loginButton.setTitle("Login", for: .normal)
                    loginButton.tag = loginButtonTag
                    if let storedUsername = UserDefaults.standard.value(forKey: "username") as? String {
                        usernameTextField.text = storedUsername
                        
                    }
                    
                }
                
            } else {
                loginButton.setTitle("Create", for: .normal)
                loginButton.tag = createLoginButtonTag
                setupAppleLogin()
            }
            
        }
        
        //MARK:- Button Actions
        @IBAction func loginAction(_ sender: UIButton) {
            
            // Check that text has been entered into both the username and password fields.
            guard let newAccountName = usernameTextField.text,
                let newPassword = passwordTextField.text,
                !newAccountName.isEmpty,
                !newPassword.isEmpty else {
                    showLoginFailedAlert()
                    return
            }
            
            usernameTextField.resignFirstResponder()
            passwordTextField.resignFirstResponder()
            
            if sender.tag == createLoginButtonTag {
                
                login.createUser(username: newAccountName, password: newPassword)
                
                loginButton.tag = loginButtonTag
                performSegue(withIdentifier: "dismissLogin", sender: self)
                
                
            } else if sender.tag == loginButtonTag {
                
                if login.checkLogin(username: newAccountName, password: newPassword) {
                    performSegue(withIdentifier: "dismissLogin", sender: self)
                } else {
                    
                    showLoginFailedAlert()
                }
            }
        }
        
        private func showLoginFailedAlert() {
            let alertView = UIAlertController(title: "Login Problem",
                                              message: "Wrong username or password.",
                                              preferredStyle:. alert)
            let okAction = UIAlertAction(title: "Foiled Again!", style: .default)
            alertView.addAction(okAction)
            present(alertView, animated: true)
            passwordTextField.text = ""
        }
        
        //MARK:- Setup Sign in with Apple Button & Helper Functions
        @available(iOS 13, *)
        func setupAppleLogin(){
            let appleButton = ASAuthorizationAppleIDButton()
            //        appleButton.translatesAutoresizingMaskIntoConstraints = false
            appleButton.addTarget(self, action: #selector(didTapAppleButton), for: .touchUpInside)
            //        view.addSubview(appleButton)
            stackView.addArrangedSubview(appleButton)
            
        }
        
        @available(iOS 13.0, *)
        @objc func didTapAppleButton(){
            let provider = ASAuthorizationAppleIDProvider()
            let request = provider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let controller = ASAuthorizationController(authorizationRequests: [request])
            controller.delegate = self
            controller.presentationContextProvider = self
            controller.performRequests()
        }
        
        func loginVIAApple(uId userIdentifier: String){
            // hasloginkey will be false first time
            if !UserDefaults.standard.bool(forKey: "hasLoginKey"){
                
                UserDefaults.standard.setValue(userIdentifier, forKey: "username")
                login.createUser(username: userIdentifier, password: userIdentifier)
                
                UserDefaults.standard.set(true, forKey: "usedAppleID")
                
                loginButton.tag = loginButtonTag
                
                performSegue(withIdentifier: "dismissLogin", sender: self)
                
            } else {
                if login.checkLogin(username: userIdentifier, password: userIdentifier){
                    performSegue(withIdentifier: "dismissLogin", sender: self)
                } else {
                    showLoginFailedAlert()
                }
            }
        }
        
    }
    
    
    //MARK:- Delegates
    
    @available(iOS 13.0, *)
    extension LoginViewController: ASAuthorizationControllerDelegate {
        
        func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
            switch authorization.credential {
            case let appleIDCredential as ASAuthorizationAppleIDCredential:
                let userIdentifier = appleIDCredential.user
                loginVIAApple(uId: userIdentifier)
                performSegue(withIdentifier: "dismissLogin", sender: self)
            default:
                break
            }
        }
        func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
            print("Error", error)
        }
        
    }
    
    @available(iOS 13.0, *)
    extension LoginViewController: ASAuthorizationControllerPresentationContextProviding{
        func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
            return view.window!
        }
    }
