//
//  MyPlacesViewController.swift
//  Fuel Log
//
//  Created by The App Experts on 08/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class MyPlacesViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!

    
    let model = FuelModel(CoreDataController.shared)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let result = model.fetchAllRecords()
        guard let testarray = result as? [Fuel] else {return}
        
        for item in testarray {

            updateMap(item: item)
            
        }


    }

    func updateMap(item: Fuel){
        let annotation = MKPointAnnotation()
        annotation.title = item.locationName
        annotation.coordinate = CLLocationCoordinate2D(latitude: item.latitude, longitude: item.longitude)
//        annotation.subtitle = item
        mapView.addAnnotation(annotation)
        
    }

}
