/*!
 @header
 @brief This file contains networking logic, talks to 2 different apis.
 */
//
//  Networking.swift
//  Fuel Log
//
//  Created by The App Experts on 07/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import Foundation

//MARK:- JSON Deconding

struct RootObject:Codable {
    var results: [Result]
}

struct Result : Codable {
    var geometry: Location
    var name: String
    var vicinity: String
}

struct Location: Codable {
    var location: LocationComponents
}

struct LocationComponents: Codable {
    var lat: Double
    var lng: Double
}

protocol NetworkSession {
    func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
    
}

extension URLSession: NetworkSession{}

class Network {
    
    lazy var session: NetworkSession = URLSession.shared
    /*!
    @brief Talks to googlemaps api to get fuelstation name withing 100 meter of device location.
    @param currentLocation curent device location, and completion handler.
    @return Decoded Rootjson object type is returned.
    */
    func search( _ searchLocation: LocationComponents, completion: @escaping(Result?, Error?) ->()) {
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host  = "maps.googleapis.com"
        urlComponents.path = "/maps/api/place/search/json"
        
        urlComponents.queryItems = [URLQueryItem(name: "location", value: "\(searchLocation.lat),\(searchLocation.lng)"),
                                    URLQueryItem(name: "radius", value: "100"),
                                    URLQueryItem(name: "types", value: "gas_station"),
                                    URLQueryItem(name: "key", value: "AIzaSyAw0m3prCKzqP-zrWauU7DsXJgMDnbQY-Y")]
        
        
        guard let url = urlComponents.url else { return print("Unable to create URL \(urlComponents)")}
        
        let dataTask = session.dataTask(with: url) {
            (data, response, error) in
            if let error = error{
                print("Error - \(error)")
                completion(nil,error)
            } else if let response = response as? HTTPURLResponse {
                switch response.statusCode {
                case 200...299:
                    if let data = data {
                        
                        let decoder = JSONDecoder()
                        do {
                            let decodedData = try decoder.decode(RootObject.self, from: data)
                            
                            guard let locationName = decodedData.results.first else {
                                completion(nil,nil)
                                return }
                            
                            completion(locationName, nil)
                            
                            
                        }
                        catch {
                            print(error)
                        }
                    }
                default:
                    print("Found \(response.statusCode)")
                }
            }
            
            
        }
        dataTask.resume()
    }
    
    
}

//MARK:- Weather API Decoding
struct Main: Codable{
    var temp: Double
    var feelsLike: Double
    var tempMin: Double
    var tempMax: Double
    var pressure: Double
    var humidity: Double
    
    enum CodingKeys: String, CodingKey{
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case temp,pressure,humidity
        
    }
}

struct Weather: Codable{
    var main: String
    var description: String
}

struct WeatherRoot: Codable{
    var weather: [Weather]
    var main: Main
}

extension Network{
    
/*!
     @brief Talks to openweathermap api to get weather information for current device location.
     @param currentLocation curent device location, and completion handler.
     @return Decoded Rootjson object type is returned.
     */
    func getWeather(currentLocation: LocationComponents, completion: @escaping(WeatherRoot?, Error?) ->()) {
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host  = "api.openweathermap.org"
        urlComponents.path = "/data/2.5/weather"
        
        urlComponents.queryItems = [URLQueryItem(name: "lat", value: "\(currentLocation.lat)"),
                                    URLQueryItem(name: "lon", value: "\(currentLocation.lng)"),
                                    URLQueryItem(name: "appid", value: "37f0bb1a511cea462f7c1f09220a4951")]
        
        
        guard let url = urlComponents.url else { return print("Unable to create URL \(urlComponents)")}
        
        let dataTask = session.dataTask(with: url) {
            (data, response, error) in
            if let error = error{
                print("Error - \(error)")
                completion(nil,error)
            } else if let response = response as? HTTPURLResponse {
                switch response.statusCode {
                case 200...299:
                    if let data = data {
                        
                        let decoder = JSONDecoder()
                        do {
                            let decodedData = try decoder.decode(WeatherRoot.self, from: data)
                            completion(decodedData, nil)
                        }
                        catch {
                            print(error)
                        }
                    }
                default:
                    print("Found \(response.statusCode)")
                }
            }
        }
        dataTask.resume()
    }
    
    
}

