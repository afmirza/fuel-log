//
//  CoreDataController.swift
//  Fuel Log
//
//  Created by The App Experts on 04/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import Foundation
import CoreData

class CoreDataController {
    
    private init() {}
    
    static let shared = CoreDataController()
    
    var mainContext: NSManagedObjectContext{
        return persistentContainer.viewContext
    }
    
    
    var privateContext: NSManagedObjectContext {
        let newConetxt = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        newConetxt.parent = mainContext
        return newConetxt
    }
    
     // MARK: - Core Data stack

        lazy var persistentContainer: NSPersistentContainer = {

            let container = NSPersistentContainer(name: "Fuel_Log")
            container.loadPersistentStores(completionHandler: { (storeDescription, error) in
                if let error = error as NSError? {

                    fatalError("Unresolved error \(error), \(error.userInfo)")
                }
            })
            return container
        }()

        // MARK: - Core Data Saving support

        func saveContext () {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {

                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        }
    
    
    /// Saving the main context
    ///
    /// - Returns: True if context was saved, else false
//    @discardableResult
//    func saveContext() -> Bool {
//        let context = persistentContainer.viewContext
//        if context.hasChanges {
//            do {
//                try context.save()
//                return true
//            } catch {
//                
//                let nserror = error as NSError
//                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//            }
//        }
//        return false
//    }

    }

