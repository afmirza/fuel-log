//
//  NetworkingTests.swift
//  Fuel LogTests
//
//  Created by The App Experts on 12/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import XCTest
@testable import Fuel_Log

class NetworkingTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testLocationResponseIsParsedSuccessfully() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        //Given
        let promis = expectation(description: "Completion handler invoked")
        let sut = Network()
        sut.session = SessionStub(json: mockJson(), error: nil)
        let location = LocationComponents(lat: 1.5, lng: 5.0)
        // When
        sut.search(location) { (string, error) in
            //then
            XCTAssertNotNil(string)
            XCTAssertNil(error)
            promis.fulfill()
            
        }
        
        wait(for: [promis], timeout: 5)
        
        
    }
    
    func testErrorIsHanlded(){
        
        //Given
        let promis = expectation(description: "Completion handler invoked")
        let sut = Network()
        let error = NSError(domain: "Something went wrong", code: 101, userInfo: nil)
        sut.session = SessionStub( error: error)
        let location = LocationComponents(lat: 1.5, lng: 5.0)
        // When
        sut.search(location) { (string, error) in
            //then
            XCTAssertNotNil(error)
            XCTAssertNil(string)
            promis.fulfill()
            
        }
        
        wait(for: [promis], timeout: 5)
    }
    
    func testLocationNameIsCorrect(){
        //Given
        let promis = expectation(description: "Completion handler invoked")
        let sut = Network()
        sut.session = SessionStub(json: mockJson(), error: nil)
        let location = LocationComponents(lat: 1.5, lng: 5.0)
        // When
        sut.search(location) { (string, error) in
            //then
            XCTAssertEqual(string?.name, "Tesco Esso Express")
            XCTAssertNil(error)
            promis.fulfill()
            
        }
        
        wait(for: [promis], timeout: 5)
    }
 
    
    
    func mockJson() -> Any{
        
        return [
            
            "results": [
                [
                    "geometry": [
                        "location": [
                            "lat": 53.79189849999999,
                            "lng": -1.8291284
                        ]
                        
                    ],
                    
                    "name": "Tesco Esso Express",
                    
                    "vicinity": "Thornton Road, Thornton, Bradford"
                ]
            ]
        ]
    }
}

class SessionStub: NetworkSession {
    
    let json : Any?
    init(json: Any? = nil, error: Error?) {
        self.json = json
        self.error = error
    }
    
    let error: Error?
    
    func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        
        if let error = error {
            completionHandler(nil,nil, error)
        }
        else{
            let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
            
            do {
                let  data = try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
                completionHandler(data,response,nil)
            } catch let myJSONError {
                
                print(myJSONError)
            }
        }

        return URLSessionDataTask()
    }
    
    
}


