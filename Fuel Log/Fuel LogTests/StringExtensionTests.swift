//
//  StringExtensionTests.swift
//  Fuel LogTests
//
//  Created by The App Experts on 12/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import XCTest
@testable import Fuel_Log

class StringExtensionTests: XCTestCase {

    var sut: String!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
    }

    func testFalseIfInputIsNotANumberNumber() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let input = "test"
        let  result = input.isValidDouble(maxDecimalPlaces: 4)
        XCTAssertFalse(result)
    }
    
    func testTrueIfValidDoubelWithOneDecimal(){
        let input = "12.08"
        let result = input.isValidDouble(maxDecimalPlaces: 4)
        XCTAssertTrue(result)
    }
    
    func testIsFalseWhenTwoDecimalsPresent(){
                let input = "12.08.09"
        let result = input.isValidDouble(maxDecimalPlaces: 4)
        XCTAssertFalse(result)

    }
    
        func testIsFalseWhenInputIsAlphanumeric(){
                let input = "12Ac.0"
        let result = input.isValidDouble(maxDecimalPlaces: 4)
        XCTAssertFalse(result)

    }
    
    func testIsFalseWhenTwoDecimalsPresent2(){
                let input = "12,00"
        let result = input.isValidDouble(maxDecimalPlaces: 4)
        XCTAssertFalse(result)

    }


}
