//
//  LoginServiceTests.swift
//  Fuel LogTests
//
//  Created by The App Experts on 12/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import XCTest
import Foundation
@testable import Fuel_Log

class LoginServiceTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        UserDefaults.standard.removeObject(forKey: "username")
        UserDefaults.standard.synchronize()
    }

    func testIfUserNameDoesNotExistReturnsFalse() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        //Given
     
        XCTAssertFalse(Login().checkLogin(username: "hello", password: "1234"))
    }
    
    func testReturnsTrueWhenUserCreated(){
          
        let sut = Login()
        let username = "hello"
        let password = "1234"
        let userCreated = sut.createUser(username: username, password: password)
        XCTAssertTrue(userCreated)

    }
    
    func testReturnsFalseWhenUserNameIsWrong(){
        let sut = Login()
        let username = "hello"
        let password = "1234"
        let userCreated = sut.createUser(username: username, password: password)
         XCTAssertFalse(sut.checkLogin(username: "username", password: "1234"))
    }

    func testReturnsFalseWhenPasswordIsWrong(){
        let sut = Login()
        let username = "hello"
        let password = "1234"
        let userCreated = sut.createUser(username: username, password: password)
        XCTAssertFalse(sut.checkLogin(username: username, password: "123"))
    }
    
    func testReturnTrueWhenUserNameAndPasswordAreCorrect(){
        let sut = Login()
        let username = "hello"
        let password = "1234"
        let userCreated = sut.createUser(username: username, password: password)
        XCTAssertTrue(sut.checkLogin(username: username, password: "1234"))
    }
}
