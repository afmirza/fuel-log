//
//  ViewControllerTests.swift
//  Fuel LogTests
//
//  Created by The App Experts on 13/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import XCTest
@testable import Fuel_Log

class ViewControllerTests: XCTestCase {

    var sut: LoginViewController!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        sut = UIStoryboard(name: "Main", bundle: nil)
        .instantiateInitialViewController() as? LoginViewController
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
    }


}
