//
//  Fuel_LogTests.swift
//  Fuel LogTests
//
//  Created by The App Experts on 04/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import XCTest
import  CoreData
@testable import Fuel_Log

class Fuel_LogTests: XCTestCase {
    var sut: CoreDataController!
    var sessionUnderTest: URLSession!
    
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        sut = CoreDataController.shared
        sessionUnderTest = URLSession(configuration: .default)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        sessionUnderTest = nil
    }

    /*this test case test for the proper initialization of CoreDataController class :)*/
      func test_init_coreDataManager(){
        
        let instance = CoreDataController.shared
        /*Asserts that an expression is not nil.
         Generates a failure when expression == nil.*/
        XCTAssertNotNil( instance )
      }
    
    /*test if NSPersistentContainer(the actual core data stack) initializes successfully
     */
    func test_coreDataStackInitialization() {
      let coreDataStack = CoreDataController.shared.persistentContainer
      
      /*Asserts that an expression is not nil.
       Generates a failure when expression == nil.*/
      XCTAssertNotNil( coreDataStack )
    }

    func testCallTosearchAllBrands() {
      // given
      let url =
        URL(string: "https://maps.googleapis.com/maps/api/place/search/json?location=53.767649,-1.5653385&radius=500&types=gas_station&sensor=false&key=AIzaSyAw0m3prCKzqP-zrWauU7DsXJgMDnbQY-Y")
      let promise = expectation(description: "Completion handler invoked")
      var statusCode: Int?
      var responseError: Error?

      // when
        let dataTask = sessionUnderTest?.dataTask(with: url!) { data, response, error in
        statusCode = (response as? HTTPURLResponse)?.statusCode
        responseError = error
        promise.fulfill()
      }
        dataTask?.resume()
      wait(for: [promise], timeout: 5)

      // then
      XCTAssertNil(responseError)
      XCTAssertEqual(statusCode, 200)
    }
    
    func testTrueIfCalculateMileageIsCorrect(){
        let k = FuelLog(pricePerLitre: 2, litres: 2, cost: 2, odometer: 50, previousOdometerValue: 30, date: Date(), locationName: "any", locationCordinates: LocationComponents(lat: 2.0, lng: 1.0))
        XCTAssertEqual(k.mileage, 10.0)
        
    }


}
