/*!
 @brief This contains model strucutre and a class that updates the core data
 */
//  FuelModel.swift
//  Fuel Log
//
//  Created by The App Experts on 04/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import Foundation
import CoreData

// struct of entity that is used to represent each fuel log
struct FuelLog {
    
    var pricePerLitre: Double
    var litres: Double
    var cost: Double
    var odometer: Double
    var previousOdometerValue: Double?
    var date: Date
    var locationName: String
    var locationCordinates: LocationComponents
    // computed property
    var mileage: Double? {
        guard let unwrapedPreviousOdomoeterValue = previousOdometerValue else{ return nil}
       return (odometer - unwrapedPreviousOdomoeterValue) / litres
    }
    
    
}

class FuelModel {
    
    let coreDataController: CoreDataController
    let network: Network
    
    // array to store retrieved objects from coreData
    private var items: [NSManagedObject] = []
    
    init(_ coreDataController: CoreDataController) {
        self.coreDataController = coreDataController
        self.network = Network()
    }
    
/*!
    @discussion This method accepts a fuel log item and saves that to coredata
     @param Takes in a FuelLog value
     @return does not return
     */
    func logFuel(item: FuelLog)  {
        
        let newLogEntry = Fuel(context: coreDataController.mainContext)
        newLogEntry.pricePerLitre = item.pricePerLitre
        newLogEntry.litres = item.litres
        newLogEntry.odometer = item.odometer
        newLogEntry.date = item.date
        newLogEntry.cost = item.cost
        newLogEntry.locationName = item.locationName
        newLogEntry.latitude = item.locationCordinates.lat
        newLogEntry.longitude = item.locationCordinates.lng
        guard let unwrappedPreviousOdometerValue = item.previousOdometerValue else {
            coreDataController.saveContext()
            return
        }
        newLogEntry.previousOdometerValue = unwrappedPreviousOdometerValue
        guard let unwrapedMileage = item.mileage else { coreDataController.saveContext()
            return
        }
        newLogEntry.mileage = unwrapedMileage

//        if let url = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).last{
//            print(url.absoluteString)
//        }
//        
        coreDataController.saveContext()

    }
  
    
    func fetchAllRecords() -> [NSManagedObject]{
    
    let fuelEntries: NSFetchRequest = Fuel.fetchRequest()
    
    fuelEntries.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]

    
    do {
    items = try coreDataController.mainContext.fetch(fuelEntries)
        
    
    } catch {
    print(error)
    }
        return items 
        
    }
    
    
}

