//
//  LoginService.swift
//  CheckLoginApple
//
//  Created by The App Experts on 11/04/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import Foundation
import UIKit

protocol UserDefaultsService {
    func value(forKey key: String) -> Any?
    
    func setValue(_ value: Any?, forKey key: String)
    func set(_ value: Bool, forKey defaultName: String)
}

extension UserDefaults: UserDefaultsService{
    
}

class Login{
    
    lazy var userDefaultService: UserDefaultsService = UserDefaults.standard
    
    func checkLogin(username: String, password: String) -> Bool {
        
        
        guard username == userDefaultService.value(forKey: "username") as? String else {
            return false
        }
        
        do {
            let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                    account: username,
                                                    accessGroup: KeychainConfiguration.accessGroup)
            let keychainPassword = try passwordItem.readPassword()
            return password == keychainPassword
        } catch {
            
            print(error.localizedDescription)
            return false
        }
        
    }
    
    func createUser(username: String, password: String) -> Bool{
        
        userDefaultService.setValue(username, forKey: "username")
        do {
            // This is a new account, create a new keychain item with the account name.
            let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                    account: username,
                                                    accessGroup: KeychainConfiguration.accessGroup)
            
            // Save the password for the new item.
            try passwordItem.savePassword(password)
        } catch {
            
            print(error.localizedDescription)
            return false
        }
        
        UserDefaults.standard.set(true, forKey: "hasLoginKey")
        
        return true
    }
}
