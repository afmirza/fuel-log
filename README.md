* The purpose of the app is to keep track of users refueling history & generate relevant statistics to help user monitor his car related expenses.
* App takes input from the user such as, date, odometer reading, price per lietre, no. of litres, total price, name of fuel station.
* Uses the above information and gives mileage and no. of miles at each refueling in charts. 
* App stores location of each refueling station which is then used to show those locations on a map.
* App allows user to setup time based reminders. 


**Specs Remaining**

* At current state app has 2 missing specs i.e. 1 third party framework and a technology new to iOS 12. 

**Missing features**
* User cannot edit fueling data. Will be nice to allow user edit the entered data.
* Allow user to add in a missed refueling by allowing an entry between two dates with relevant odometer reading.


**Bugs**
* Reminders work fine on iOS 13 but, Reminders donot trigger an alarm in iOS12 despite being added to the EventKit database.
* Editing reminders changes title but it doesnot change the time and doesnot triiger the alarm, on both iOS 12 and 13


**Nice to haves**

* Refactoring code, project was put together in 2 weeks time, hence a lot of room of code refactoring
* Use of map api in a better way
* More unit testing
* More statistical data
* Setting up location based reminders
* Reminder/notification based on fuel log history, such as expected fueling in xxx number of days
* Improved/better use of design patterns
* Anything you can think of that should be in a fuel loggin app. 

**Fixes**
* update tabBar icons for a better fit
* password recovery
* Edit Fuel data
* prevent entering a fuel data with no location
* tidy up display of date in Fule Log 

